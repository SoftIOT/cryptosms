package pce.marko.smscypto;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

   TextView phone;
   EditText phone_text, sms_text;
   Button btn_send;
   ToggleButton tglbtn;
   Button encrypt_btn, decrypt_btn;
    String password_crypto = "crypto3*pass";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        phone = (TextView)findViewById(R.id.textView);
        phone_text = (EditText)findViewById(R.id.editTextPhone);
        sms_text = (EditText)findViewById(R.id.editTextSMS);
        btn_send = (Button)findViewById(R.id.button_sms);

        encrypt_btn = (Button)findViewById(R.id.button_enc);
        decrypt_btn = (Button)findViewById(R.id.button_dec);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNum = phone_text.getText().toString();
                String smstext = sms_text.getText().toString();

                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNum, null, smstext, null, null);
                }
                catch (Exception exp){
                    exp.printStackTrace();
                }

                Log.d("SMS PHONE ", phoneNum + " : " + smstext);
            }
        });

        final byte[] encrypted_decrypted = {};
        // final byte[] clear = " {id:3, name:Daria, age:22, country:Rusia}".getBytes();
        // final String seed = "my secret key password";

        encrypt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // final byte[] clear = " {id:3, name:Daria, age:22, country:Rusia}".getBytes();
               final byte[] clear = "1*aes sms text*3".getBytes();

                Log.d("3. ", " -- Security Util ---------------------------------- ");
                try{

                    /* byte[] keyraw = AES.keyRawEncoded();
                    Log.d("key", new String(keyraw));

                    // -> Log.d("clearValue ", new String(clear));
                    Log.d("clearValue ", new String(sms_text.getText().toString()));
                    // -> byte[] encrypted = AES.encrypt(keyraw, clear);
                    byte[] encrypted = AES.encrypt(keyraw, sms_text.getText().toString().getBytes());

                    Log.d("encrypted", new String(encrypted));
                    Log.d("encrypted hex", bytesToHex(encrypted));

                    sms_text.getText().clear();
                    sms_text.setText(bytesToHex(encrypted));
                    // Log.d("encrypted hex dec", sms_text.getText().toString());
                    // Log.d("encrypted hex dec1", new String(bytesToHex(encrypted)));

                    // byte[] decrypted = AES.decrypt(keyraw, encrypted);
                    // byte[] decrypted = AES.decrypt(keyraw, sms_text.getText().toString().getBytes());
                    byte[] decrypted = AES.decrypt(keyraw,toByte(sms_text.getText().toString()));
                    // TEK KAD SE SADRZAJ ENKODIRA U BAJT ARRAY SA toByte() ONDA JE RADILO ISPRAVNO!
                    // SAMO sms_text.getText().toString().getBytes() NE RADI ENKODIRANJE SADRZAJA
                    Log.d("decrypted", new String(decrypted)); */

                    byte[] keyraw = AES.keyRawEncoded();
                    Log.d("key", new String(keyraw));

                    // -> Log.d("clearValue ", new String(clear));
                    Log.d("clearValue ", new String(sms_text.getText().toString()));
                    // -> byte[] encrypted = AES.encrypt(keyraw, clear);
                    byte[] encrypted = AES.encrypt(keyraw, sms_text.getText().toString().getBytes());
                    Log.d("encrypted", new String(encrypted));
                    byte[] encrypted_64 = Base64.encode(encrypted, Base64.DEFAULT);
                    // ENKRIPTIRANI CIPHER DA BI SE UPISAO U TEXTVIEW PA KASNIJE PROCITAO IZ TEXTVIEW-A ZA DEKRIPCIJU
                    // TREBA SE PRVO ENKODIRAT SA Base64.encode(). KAD SE CITA IZ TEXTVIEW-A ZA DEKRIPICIJU TADA SE TREBA
                    // TREBA DEKODIRAT SA Base64.decode() PA ONDA DEKRIPTIRAT.
                    // KAD NIJE BILO Base64.encode() - Base64.decode() javljalo se ILLEGALBLOCK, NOPADDINGBLOCK ...
                    Log.d("encrypted 64", new String(encrypted_64));
                    // Log.d("encrypted hex", bytesToHex(encrypted));

                    sms_text.getText().clear();
                    // sms_text.setText(bytesToHex(encrypted));
                    sms_text.setText(new String(encrypted_64));
                    // Log.d("encrypted hex dec", sms_text.getText().toString());
                    // Log.d("encrypted hex dec1", new String(bytesToHex(encrypted)));

                    // byte[] decrypted = AES.decrypt(keyraw, encrypted);
                    // byte[] decrypted = AES.decrypt(keyraw, sms_text.getText().toString().getBytes());
                    String decrypt_decode = sms_text.getText().toString();
                    Log.d("decrypt_decode ", decrypt_decode);
                    byte[] decrypt_64 = Base64.decode(decrypt_decode.getBytes(), Base64.DEFAULT);
                    Log.d("decrypt_decode 64 ", new String(decrypt_64));
                    // Log.d("decrypt_decode ", decrypt_decode);
                    // byte[] decrypted = AES.decrypt(keyraw,toByte(sms_text.getText().toString()));
                    byte[] decrypted = AES.decrypt(keyraw, decrypt_64);
                    // TEK KAD SE SADRZAJ ENKODIRA U BAJT ARRAY SA toByte() ONDA JE RADILO ISPRAVNO!
                    // SAMO sms_text.getText().toString().getBytes() NE RADI ENKODIRANJE SADRZAJA
                    Log.d("decrypted", new String(decrypted));

                }
                catch (Exception exp){
                    Log.e("EncDec err ", exp.getMessage());
                }

                /* try{
                    Log.d("key ", new String("my secret key password"));
                    Log.d("clear text ", new String(clear));
                    MyCipher myCipher = new MyCipher("my secret key password");
                    MyCipherData myCipherData = myCipher.encryptUFT8(new String(clear));
                    byte[] encrypted = myCipherData.getData();
                    Log.d("encrypted ", new String(encrypted));
                    Log.d("encrypted hex", bytesToHex(encrypted));

                    sms_text.setText("");
                    sms_text.setText(new String(bytesToHex(encrypted)));

                    IvParameterSpec iv = new IvParameterSpec(myCipherData.getIv());
                    // String decrypt = myCipher.decryptUTF8(encrypted, iv);
                     // String decrypt = myCipher.decryptUTF8(sms_text.getText().toString().getBytes(), iv);
                    String decrypt = myCipher.decryptUTF8(toByte(sms_text.getText().toString()), iv);
                    Log.d("sms_text ", sms_text.getText().toString());
                    byte[] resdec = toByte(sms_text.getText().toString());
                    Log.d("decrypted bytes", new String(resdec));
                    Log.d("sms_text hex", bytesToHex(sms_text.getText().toString().getBytes()));
                    Log.d("decrypted ", new String(decrypt));
                }
                catch (Exception exp){
                    Log.e("EncDec err ", exp.getMessage());
                } */
                /* try {

                    // byte[] rawkey1 = aesEncryptionDecryption.getkeyRawEncoded();
                    // String seed = "key aes password";
                    // Log.d("Key ", new String(seed));
                    // byte[] rawkey1 = SecurityUtils.getRawKey(seed.getBytes());
                    // Log.d(TAGkey, new String(rawkey1));

                    // Log.d("clearValue ", new String("this is example.".getBytes()));
                    // Log.d("clearValue ", new String(clear));
                    Log.d("clearValue ", sms_text.getText().toString());
                    // byte[] encrypted = aesEncryptionDecryption.getEncrypt(rawkey1, clear);
                   // byte[] encrypted = SecurityUtils.encryptAES(new String(rawkey1), new String(clear));

                    // String byteBase64 = Base64.encodeToString(sms_text.getText().toString().getBytes(), Base64.DEFAULT);
                    byte[] encrypted = SecurityUtils.encryptAES(seed, new String(clear));
                    Log.d("ENCRYPT ", new String(encrypted));
                    // sms_text.getText().clear();
                      sms_text.setText("");
                      sms_text.setText(new String(encrypted));
                    Log.d("encrypted ", new String(encrypted));
                    Log.d("encrypted hex", bytesToHex(encrypted));
                    Log.d("sms_text hex", bytesToHex(sms_text.getText().toString().getBytes()));

                    // System.arraycopy(encrypted, 0, encrypted_decrypted, 0, encrypted.length);
                    // byte[] decrypted = SecurityUtils.decryptAES(new String(rawkey1), encrypted);
                    // -> byte[] decrypted = SecurityUtils.decryptAES(seed, encrypted);
                    // byte[] byteBase64decoded = Base64.decode(encrypted, Base64.DEFAULT);
                    // byte[] decrypted = SecurityUtils.decryptAES(seed, encrypted);
                    // byte[] decrypted = SecurityUtils.decryptAES(seed, sms_text.getText().toString().getBytes());
                    byte[] decrypted = SecurityUtils.decryptAES(seed, toByte(sms_text.getText().toString()));
                    Log.d("DECRYPT ", new String(decrypted));

                }
                catch (Exception exp){
                    Log.e("EncDec err ", exp.getMessage());
                } */
                /* try {
                    //// byte[] encrypted = SecurityUtils.encryptAES(password_crypto, sms_text.getText().toString());
                    byte[] encrypted = SecurityUtils.encryptAES(seed, new String(clear));
                    // System.arraycopy(encrypted, 0, encrypted_decrypted, 0, encrypted.length);
                    sms_text.getText().clear();
                    sms_text.setText(new String(encrypted));
                    Log.d("Encrypted ", new String(encrypted));
                    // Log.d("Encrypted_Decrypted ", new String(encrypted_decrypted));
                } catch (Exception e) {
                    e.printStackTrace();
                } */

            }
        });

        decrypt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ContentResolver contentResolver = getContentResolver();
                Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null);
                int indexBody = smsInboxCursor.getColumnIndex("body");
                int indexAddress = smsInboxCursor.getColumnIndex("address");

                 if(indexBody < 0 || !smsInboxCursor.moveToFirst())
                     Log.d("smsInboxCursor ", "error");

                String str = null;
                String[] messages = new String[30];
                int count = 0;
                do{
                    str += "Sms from " + smsInboxCursor.getString(indexAddress) + "\n" + smsInboxCursor.getString(indexBody) + "\n";
                    messages[count] = smsInboxCursor.getString(indexBody);
                    count++;
                }while(smsInboxCursor.moveToNext());
                // Log.d("SMS ", str);
                Log.d("ZADNJA ", messages[count-1]);


                try{
                    byte[] keyraw = AES.keyRawEncoded();
                    Log.d("key btn", new String(keyraw));

                    /* byte[] decrypted = AES.decrypt(keyraw,toByte(sms_text.getText().toString()));
                    Log.d("decrypted btn", new String(decrypted)); */

                    byte[] decrypt_64 = Base64.decode(sms_text.getText().toString().getBytes(), Base64.DEFAULT);
                    Log.d("decrypt_decode 64 ", new String(decrypt_64));
                    // Log.d("decrypt_decode ", decrypt_decode);
                    // byte[] decrypted = AES.decrypt(keyraw,toByte(sms_text.getText().toString()));
                    byte[] decrypted = AES.decrypt(keyraw, decrypt_64);
                    // TEK KAD SE SADRZAJ ENKODIRA U BAJT ARRAY SA toByte() ONDA JE RADILO ISPRAVNO!
                    // SAMO sms_text.getText().toString().getBytes() NE RADI ENKODIRANJE SADRZAJA
                    Log.d("decrypted", new String(decrypted));


                    sms_text.getText().clear();
                    sms_text.setText(new String(decrypted));


                }
                catch(Exception e){
                    e.printStackTrace();
                }
                /* try {
                    Log.d("Decrypted ", sms_text.getText().toString());
                    // byte[] decrypted = SecurityUtils.decryptAES(password_crypto, sms_text.getText().toString());
                    // byte[] decrypted = SecurityUtils.decryptAES(seed, new String(clear));
                    // Log.d("Decrypted ", new String(decrypted));

                    Log.d("ENCDEC ", sms_text.getText().toString());
                    // byte[] decrypted = SecurityUtils.decryptAES(seed, sms_text.getText().toString().getBytes());
                    // Log.d("DECRYPT ", new String(decrypted));

                } catch (Exception e) {
                    e.printStackTrace();
                } */
            }
        });

    }

    public static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2),
                    16).byteValue();
        return result;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public void onToggleClicked(View view){

        boolean on = ((ToggleButton)view).isChecked();

         if(on) {
             Toast.makeText(this, "toggle encrypt", Toast.LENGTH_SHORT).show();
         }
        else{
             Toast.makeText(this, "toggle normal", Toast.LENGTH_SHORT).show();
         }

    }



}
