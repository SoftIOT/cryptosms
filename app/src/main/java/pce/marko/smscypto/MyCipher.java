package pce.marko.smscypto;

/**
 * Created by Davor on 5/11/2017.
 */

import android.util.Base64;
import android.util.Log;

import java.nio.charset.Charset;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
// import org.apache.commons.codec.binary.Base64;
/**
 * Created by Davor on 5/4/2017.
 */

public class MyCipher {

    private final static String ALGORITHM = "AES";
    private String mySecret;

    public MyCipher(String mySecret){
        this.mySecret = mySecret;
    }

    public MyCipherData encryptUFT8(String data) {
        try {
            // -> Log.d("encryptUTF8 ", data);
            // byte[] bytes = data.getBytes();
            byte[] bytes = data.toString().getBytes();
            // byte[] bytesBase64 = Base64.encodeBase64(bytes);
            byte[] bytesBase64 = Base64.encode(bytes, Base64.DEFAULT);
            // Log.d("Base64 ", new String(bytesBase64, Charset.forName("UTF-8")));
            // -> Log.d("Base64 ", new String(bytesBase64));
            // String byteBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
            return encrypt(bytesBase64);

        }
        catch(Exception exp){
            Log.d("Base64 encode", exp.getMessage());
            return null;
        }
    }

    public String decryptUTF8(byte[] encryptedData, IvParameterSpec iv){
        try {
            // -> Log.d("Decrypt data", new String(encryptedData) );

            byte[] decryptedData = decrypt(encryptedData, iv);
            // -> Log.d("Decrypt data decrypt", new String(decryptedData) );
            byte[] decodedBytes = Base64.decode(decryptedData, Base64.DEFAULT);
            // String restored_bytes = new String(decodedBytes, Charset.forName("UTF8"));
            String restored_bytes = new String(decodedBytes);
            // -> Log.d("Decrypt result ", restored_bytes);
            return restored_bytes;


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Base64 encode", e.getMessage());
            return null;
        }
    }

    // private MyCipherData encrypt(byte[] raw, byte[] clear){
    private MyCipherData encrypt(byte[] clear)throws Exception{

        byte[] rawKey = getKey();
        // -> Log.d("Secret key enc", new String(rawKey));

        SecretKeySpec skeySpec = new SecretKeySpec(rawKey, ALGORITHM);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] data = cipher.doFinal(clear);

        AlgorithmParameters params = cipher.getParameters();
        byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();

        return new MyCipherData(data, iv);

    }

    // private byte[] decrypt(byte[] raw, byte[] encrypted, IvParameterSpec iv)throws Exception{
    private byte[] decrypt(byte[] encrypted, IvParameterSpec iv)throws Exception{

        byte[] rawKey = getKey();
        // Log.d("Secret key dec", new String(rawKey));

        SecretKeySpec skeySpec = new SecretKeySpec(rawKey, ALGORITHM);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;

    }

    private byte[] getKey() throws Exception{

        // -> Log.d("getKey ", this.mySecret.toString());
        byte[] keyStart = this.mySecret.getBytes();
        KeyGenerator kgen = KeyGenerator.getInstance(ALGORITHM);
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
        sr.setSeed(keyStart);
        kgen.init(128, sr);
        SecretKey skey = kgen.generateKey();
        byte[] key = skey.getEncoded();
        // -> Log.d("Key return ", new String(bytesToHex(key)));
        return key;
    }


    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}

