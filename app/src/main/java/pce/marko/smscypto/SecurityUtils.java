package pce.marko.smscypto;

import android.util.Base64;
import android.util.Log;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Davor on 5/10/2017.
 */

public class SecurityUtils {

    private final static int BLOCKS = 192;

    public static byte[] encryptAES(String seed, String cleartext) throws Exception{

        byte[] rawkey = getRawkey(seed.getBytes("UTF8"));
        // byte[] cleartext_bytes = cleartext.toString().getBytes();
        // byte[] cleartext64 = Base64.encode(cleartext_bytes, Base64.DEFAULT);
        // Log.d("Key ", new String(seed));
        // Log.d("Secret Key ", new String(rawkey));
        SecretKeySpec skeySpec = new SecretKeySpec(rawkey, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        // Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); // java.security.InvalidKeyException: no IV set when one expected
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        // byte[] encrypt = cipher.doFinal(cleartext.getBytes("UTF8"));
        byte[] encrypt = cipher.doFinal(cleartext.getBytes());
        return encrypt;
    }

    // public static byte[] decryptAES(String seed, String encrypted) throws Exception{
    // public static byte[] decryptAES(String seed, String encrypted)throws Exception{
    public static byte[] decryptAES(String seed, byte[] encrypted)throws Exception{
        byte[] rawkey = getRawkey(seed.getBytes("UTF8"));
         SecretKeySpec skeySpec = new SecretKeySpec(rawkey, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        // Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        // byte[] decrypt = cipher.doFinal(encrypted.getBytes("UTF8"));
        // byte[] decrypt = cipher.doFinal(encrypted.getBytes());
        byte[] decrypt = cipher.doFinal(encrypted);
        // byte[] decodedBytes = Base64.decode(decrypt, Base64.DEFAULT);
        // return  decodedBytes;
         return decrypt;
        // return null;
    }

    // public static byte[] getRawkey(byte[] seed) throws Exception{
    private static byte[] getRawkey(byte[] seed) throws Exception{

        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");

        sr.setSeed(seed);
        kgen.init(BLOCKS, sr);
        SecretKey skey = kgen.generateKey();
        byte[] raw = skey.getEncoded();

        Log.d("Key ", new String(seed));
        Log.d("Secret Key ", new String(raw));

        return raw;
    }

}
