package pce.marko.smscypto;

/**
 * Created by Davor on 5/11/2017.
 */

public class MyCipherData{

    private byte[] data;
    private byte[] iv;

    public MyCipherData(byte[] data, byte[] iv){
        this.data = data;
        this.iv = iv;
    }

    public byte[] getData(){
        return data;
    }

    public byte[] getIv(){
        return iv;
    }
}


